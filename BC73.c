#include<stdio.h>
int main()
{
    int a = 0;
    while (scanf("%d", &a) != EOF)
    {
        if (a >= 0 && a < 4)
            printf("Good\n");
        else if (a >= 4 && a < 10)
            printf("Danger\n");
        else
            printf("Danger++\n");
    }
    return 0;
}