#include<stdio.h>
#include<math.h>
int main()
{
    float a, b, c;
    float d = 0;
    while (scanf("%f %f %f", &a, &b, &c) != EOF)
    {
        d = b * b - (4 * a * c);
        float x, y;//x为实部，y为虚部
        x = -b / (2 * a);
        y = sqrt(-d) / (2 * a);
        float z = sqrt(d);
        if (a == 0)
            printf("Not quadratic equation\n");
        else if (d == 0)
            printf("x1=x2=%.2f\n", (-b) / (2 * a));
        else if (d > 0)
            printf("x1=%.2f;x2=%.2f\n", (-b - z) / (2 * a), (-b + z) / (2 * a));
        else
            printf("x1=%.2f-%.2fi;x2=%.2f+%.2fi", x, y, x, y);
    }
    return 0;
}