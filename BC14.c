#include<stdio.h>
int main()
{
    int a = 0;
    int y = 0;
    int m = 0;
    int d = 0;
    scanf("%d", &a);
    y = a / 10000;
    d = a % 100;
    m = (a % 10000 - d) / 100;
    printf("year=%04d\n", y);
    printf("month=%02d\n", m);
    printf("date=%02d", d);
    return 0;
}