#include<stdio.h>
#include<math.h>
int main()
{
    int height, weight;
    double BMI;
    while (scanf("%d %d\n", &weight, &height) != EOF)
    {
        //height=height;
        BMI = (weight / (height / 100.0 * height / 100.0));
        if (BMI < 18.5)
            printf("Underweight\n");
        else if (BMI >= 18.5 && BMI <= 23.9)
            printf("Normal\n");
        else if (BMI > 23.9 && BMI <= 27.9)
            printf("Overweight\n");
        else
            printf("Obese\n");
    }
    return 0;
}