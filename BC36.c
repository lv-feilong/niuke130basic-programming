#include<stdio.h>
#include<math.h>
int main()
{
    double weight = 0;
    double height = 0;

    while (scanf("%lf %lf", &weight, &height) != EOF)
    {
        double bmi = 0;
        bmi = weight / pow(height, 2);
        bmi <= 23.9 && bmi >= 18.5 ? printf("Normal\n") : printf("Abnormal\n");
    }
    return 0;
}